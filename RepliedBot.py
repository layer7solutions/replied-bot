import configparser
import logging
import logging.config
import praw
import prawcore
import psycopg2
import sys
import urllib3
from datetime import datetime
from layer7_utilities import LoggerConfig, oAuth
from requests.exceptions import Timeout
from time import sleep, time

from _version import __version__

repliedbot = configparser.ConfigParser()
repliedbot.read("botconfig.ini")

__botname__ = "RepliedBot"
__description__ = "Posts Employee Replied to posts and flairs accordingly"
__author__ = "u/Try_to_guess_my_psn"
__dsn__ = repliedbot.get("BotConfig", "DSN")
__logpath__ = repliedbot.get("BotConfig", "logpath")

# ~~~~~~~~~~ Import Config Settings ~~~~~~~~~~ #
DB_USERNAME = repliedbot.get("Database", "Username")
DB_PASSWORD = repliedbot.get("Database", "Password")
DB_HOST = repliedbot.get("Database", "Host")
DatabaseName = "Zion"
# ~~~~~~~~~~ Import Config Settings ~~~~~~~~~~ #


DEFAULT_VALUES = {
    # REQUIRED VALUES (error when missing)
    # 'TEXT': "This is a list of links to comments made by Bungie employees in this thread:",
    # DEFAULT VALUES (will default to value shown here if omitted)
    "FLAIR": False,
    "FLAIR_COUNT": False,  # requires FLAIR_TEXT_ADD_TO_END to be True
    "FLAIR_TEXT": "",
    "FLAIR_TEXT_ADD_TO_END": True,
    "FLAIR_TEXT_REPLACE": False,  # overwrites FLAIR_TEXT_ADD_TO_END
    "FLAIR_CSS_FUNC": None,  # not redesign compatible (flair id get's deleted)
    "FLAIR_ID": None,
    "MINCOMMENTLEN": 0,
    "TRUNCATELEN": 300,
    "TRIGGER_FLAIR_TEXT": None,
    "TRIGGER_FLAIR_CLASS": None,
    "TRIGGER_COMMENT_TEXT": None,
    "TRIGGER_ON_OP": False,
    "LOCK_COMMENT": False,
    "REPLACE_STICKY_COMMENT": False,
}


class RepliedBot(object):
    def __init__(self, subreddit, agent, config):
        self.subreddit_name = subreddit
        self.config = {**DEFAULT_VALUES, **config}
        self.agent = agent
        self.r = None
        self.me = None

        if subreddit.lower() == "destinythegame":
            self.magicword = "[](#AUTOGEN_BUNGIEREPLY)"
        else:
            self.magicword = "[](#AUTOGEN_TSBREPLIEDBOT)"

        # ~~~~~~~~~~ LOGGER SETUP ~~~~~~~~~~ #
        # Setup logging
        loggerconfig = LoggerConfig(__dsn__, __botname__, __version__, __logpath__)
        logging.config.dictConfig(loggerconfig.get_config())
        self.logger = logging.getLogger("root")
        self.logger.info("/*********Starting App*********\\")
        self.logger.info(f"Version: {__version__}")
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

        if "TEXT" not in self.config:
            self.logger.critical("Required value TEXT missing from config")
            sys.exit()

        conn = psycopg2.connect(
            host=DB_HOST, dbname=DatabaseName, user=DB_USERNAME, password=DB_PASSWORD
        )
        conn.autocommit = True
        self.c = conn.cursor()
        self.logger.info("Connected to DB")

        self.canLaunch = self.login()
        if not self.canLaunch:
            return
        self.sub = self.r.subreddit(self.subreddit_name)
        self.own_comments = {}

        self.launch()

    def login(self):
        try:
            auth = oAuth()
            auth.get_accounts(self.agent, __description__, __version__, __author__, __botname__, DB_USERNAME, DB_PASSWORD, DB_HOST, "TheTraveler")
            for account in auth.accounts:
                self.r = account.login()
            self.me = self.r.user.me()
            self.logger.info(f"Connected to account: {self.r.user.me()}")
            return True
        except:
            self.logger.exception(f"{self.agent} | Failed to log in.")
            return False

    def getOwnComment(self, submission):
        if submission.id not in self.own_comments:
            self.logger.info("{} | No RepliedBot comment yet, making one".format(self.me))

            # WARNING: header should always end with ":\n\n\n\n"
            own_comment = submission.reply(
                f"{self.config['TEXT']}\n\n\n\n---\nThis is a bot providing a service. If you have any questions, please [contact the moderators](https://www.reddit.com/message/compose?to=%2Fr%2F{self.subreddit_name}).{self.magicword}"
            )
            own_comment.disable_inbox_replies()
            self.own_comments[submission.id] = own_comment.id

            # sticky logic
            stickyself = True
            submission.comments.replace_more(limit=0)
            for top_level_comment in submission.comments:
                if top_level_comment.stickied:
                    if self.config['REPLACE_STICKY_COMMENT'] is False:
                        own_comment.report("Already stickied comment. Ask u/D0cR3d if you want bot to auto replace instead.")
                        stickyself = False
                        break
            own_comment.mod.distinguish(how="yes", sticky=stickyself)

            if self.config["LOCK_COMMENT"]:
                own_comment.mod.lock()
        else:
            own_comment = self.r.comment(id=self.own_comments[submission.id])
        self.logger.info(
            f"{self.me} | RepliedBot comment is ID {own_comment.id}")

        return own_comment

    def parseComment(self, body):
        # process user comment body
        if self.config["TRUNCATELEN"] > 0 and len(body) > self.config["TRUNCATELEN"]:
            body = body[: self.config["TRUNCATELEN"]] + "..."
        body = body.strip().replace("\n", "\n >")

        return body

    def editFlair(self, submission, body):
        if not self.config["FLAIR"]:
            return

        try:
            flair_id = submission.link_flair_template_id
        except AttributeError:
            flair_id = None
        flairtext = submission.link_flair_text
        flairclass = submission.link_flair_css_class


        if flairtext is None or self.config['FLAIR_TEXT_REPLACE']:
            flairtext = ""
        if self.config["FLAIR_TEXT"] not in flairtext:
            if self.config["FLAIR_TEXT_ADD_TO_END"]:
                flairtext = flairtext + self.config["FLAIR_TEXT"]
            else:
                flairtext = self.config["FLAIR_TEXT"] + flairtext

        if self.config["FLAIR_COUNT"] and self.config["FLAIR_TEXT_ADD_TO_END"]:
            # update/set count in flair
            f = flairtext.split(" ")
            count = body.count("[Comment by ")
            if "x" in f[-1]:
                f[-1] = "x{}".format(count)
            else:
                f.append("x{}".format(count))
            if count > 1:
                flairtext = " ".join(f)

        if self.config["FLAIR_CSS_FUNC"]:
            new_flairclass = self.config["FLAIR_CSS_FUNC"](flairclass)
            if new_flairclass != flairclass:
                flairclass = new_flairclass
                flair_id = None  # Force old design flairs

        # Mostly overwrites custom css changes (above), but well.. should work :)
        if self.config["FLAIR_ID"]:
            flair_id = self.config["FLAIR_ID"]

        if len(flairtext) == 0:
            flairtext = None

        if (flairtext == submission.link_flair_text and flairclass == submission.link_flair_css_class):
            # No changes
            return

        if flair_id is None:  # old design
            submission.mod.flair(text=flairtext, css_class=flairclass)
        else:  # redesign
            submission.flair.select(flair_id, text=flairtext)

    def check_comment_trigger(self, comment):
        if self.config["TRIGGER_FLAIR_TEXT"] and (
            comment.author_flair_text is not None
            and self.config["TRIGGER_FLAIR_TEXT"] in comment.author_flair_text
        ):
            return True

        if self.config["TRIGGER_FLAIR_CLASS"] and (
            comment.author_flair_css_class is not None
            and (
                (
                    isinstance(self.config["TRIGGER_FLAIR_CLASS"], tuple)
                    and comment.author_flair_css_class
                    in self.config["TRIGGER_FLAIR_CLASS"]
                )
                or (
                    not isinstance(self.config["TRIGGER_FLAIR_CLASS"], tuple)
                    and self.config["TRIGGER_FLAIR_CLASS"]
                    in comment.author_flair_css_class
                )
            )
        ):
            return True

        if self.config["TRIGGER_COMMENT_TEXT"] and (
            comment.body is not None
            and self.config["TRIGGER_COMMENT_TEXT"] in comment.body
        ):
            return True
        if self.config["TRIGGER_ON_OP"] and comment.is_submitter:
            return True

        return False

    def getCommentsFromThread(self, submission):
        commentList = []
        self.logger.info("{} | Getting all Employee comments for {}".format(self.me, submission.fullname))
        submission.comments.replace_more(limit=None)
        for comment in submission.comments.list():
            try:
                if comment.removed:
                    continue
            except AttributeError:
                self.logger.warn(
                    "self.getCommentsFromThread: {} | missing mod permissions in /r/{}, can't see `removed` attribute on comment: {} | post: {}".format(
                        self.me, submission.subreddit.display_name, comment.fullname, submission.fullname
                    )
                )
                return

            # own comment
            if comment.author == self.r.user.me() and self.magicword in comment.body:
                self.logger.debug("{} | Found an own comment {}".format(self.me, comment.fullname))
                self.own_comments[submission.id] = comment.id
                continue
            # check if comment triggers the bot
            append = self.check_comment_trigger(comment)

            if not append:
                continue
            commentList.append(comment)
        commentList.sort(key=lambda comment: comment.created_utc)

        # generate own comment content
        own_comment = self.getOwnComment(submission)
        body = ""
        for comment in commentList:
            comment_body = self.parseComment(comment.body)
            if len(comment_body) < self.config["MINCOMMENTLEN"]:
                continue

            timestamp = datetime.utcfromtimestamp(comment.created_utc).strftime("%Y-%m-%d %H:%M:%S")
            body += f'* [Comment by {comment.author.name}]({comment.permalink}?context=8 "posted on {timestamp} UTC"):\n\n > {comment_body}\n\n'

        # edit own_comment
        own_body = own_comment.body
        footerpos = own_body.find("\n---")
        headerpos = own_body.find(":") + 4
        if headerpos == -1:
            self.logger.warning("{} | RepliedBot comment found, but missing ':' after the header?!".format(self.me))
            return
        if footerpos == -1:
            self.logger.warning("{} | RepliedBot comment found, but missing '---' for the footer?!".format(self.me))
            return
        own_body = "{}{}{}".format(
            own_body[:headerpos], body, own_body[footerpos:])
        own_comment.edit(own_body)

        self.editFlair(submission, own_body)

    def checkNewComments(self):
        last = self.last_checked_comment
        self.logger.debug("{} | Getting New comments in {} (since {})".format(self.me, self.subreddit_name, last))

        for comment in self.sub.comments(limit=300):
            # Skip processed comments
            if comment.created_utc <= last:
                continue
            # check if comment should be processed
            process = self.check_comment_trigger(comment)
            if not process:
                continue

            if comment.created_utc > self.last_checked_comment:
                self.last_checked_comment = comment.created_utc

            self.logger.info("{} | Found new Employee comment (ID {})".format(self.me, comment.fullname))

            # length check
            body = self.parseComment(comment.body)
            if len(body) < self.config["MINCOMMENTLEN"]:
                comment.report("Employee replied, but below {} chars. Check if relevant".format(self.config["MINCOMMENTLEN"]))
                continue

            post = comment.submission
            own_comment = self.getOwnComment(post)

            # process bot comment body
            own_body = own_comment.body
            footerpos = own_body.find("\n---")
            if footerpos == -1:
                self.logger.warning("{} | RepliedBot comment found, but missing '---' for the footer?!".format(self.me))
                continue

            timestamp = datetime.utcfromtimestamp(comment.created_utc).strftime("%Y-%m-%d %H:%M:%S")
            own_body = f'{own_body[:footerpos]}* [Comment by {comment.author.name}]({comment.permalink}?context=8 "posted on {timestamp} UTC"):\n\n > {body}\n\n{own_body[footerpos:]}'
            if len(own_body) > 10000:
                comment.report("too many replies, unable to include in reply bot comment (10k char limit)")
                continue
            own_comment.edit(own_body)

            self.editFlair(post, own_body)

            self.logger.info("{} | Processed comment".format(self.me))

        self.logger.debug("{} | Last checked comment was at {}".format(self.me, self.last_checked_comment))
        # update database
        if last != self.last_checked_comment:
            self.updateLastChecked()

        self.logger.debug("{} | Sleeping for 60 seconds.".format(self.me))
        sleep(60)

    def updateLastChecked(self):
        try:
            self.c.execute('SELECT 1')
        except psycopg2.OperationalError:
            conn = psycopg2.connect(host=DB_HOST, dbname=DatabaseName, user=DB_USERNAME, password=DB_PASSWORD)
            conn.autocommit = True
            self.c = conn.cursor()

        self.logger.debug("{} | Updating last_checked_comment".format(self.me))
        self.c.execute(
            "UPDATE tsb_repliedbot SET last_checked_comment = to_timestamp(%d) AT TIME ZONE 'UTC' WHERE subreddit_name = '%s'"
            % (self.last_checked_comment, self.subreddit_name)
        )

    def checkDMs(self):
        for msg in self.r.inbox.unread(limit=50):
            # Only handle messages (DMs)
            if not isinstance(msg, praw.models.reddit.message.Message):
                continue
            if (not msg.subject == "TSBRepliedBot" and not msg.subject == "BungieReplied"):
                continue
            msg.mark_read()
            if msg.author not in self.r.subreddit(self.subreddit_name).moderator():
                self.logger.warning("{} | [DM] Got DM from non-mod {} - {}".format(self.me, msg.author, msg.fullname))
                continue
            parts = msg.body.split(" ")
            try:
                submission = self.r.submission(url=parts[0])
            except praw.exceptions.ClientException:
                self.logger.exception("{} | [DM] Couldn't get details for submission - {}".format(self.me, msg.fullname))
                msg.reply("Sorry, you provided an invalid URL ({}).".format(parts[0]))
                continue

            try:
                self.getCommentsFromThread(submission)
                msg.reply("{} | Success! I've updated {} with the Employee comments.".format(self.me, parts[0]))
            except Exception:
                self.logger.exception("{} | [DM] Error posting RepliedBot comment - {}".format(self.me, msg.fullname))
                msg.reply("An error occurred making the comment. Please contact r/Layer7 support via mod mail or [discord](https://discord.gg/XfYcVhH)")

    def launch(self):
        if self.canLaunch:
            self.logger.info("{} | Getting old RepliedBot comments".format(self.me))
            for comment in self.r.user.me().comments.new(limit=300):
                try:
                    if comment:
                        if comment.subreddit.display_name.lower() == self.subreddit_name.lower():
                            if comment.removed:
                                continue
                        else:
                            continue
                except AttributeError:
                    self.logger.debug("self.launch: {} | Missing mod permissions in /r/{}, can't 'removed' attribute on comment {}".format(self.me, comment.subreddit.display_name, comment.fullname))
                    continue

                if self.magicword in comment.body:
                    self.own_comments[comment.submission.id] = comment.id

            self.logger.info("{} | Getting last checked comment time".format(self.me))
            self.c.execute(
                "SELECT extract(epoch from last_checked_comment) FROM tsb_repliedbot WHERE subreddit_name = '%s'"
                % (self.subreddit_name)
            )
            fetch = self.c.fetchone()
            if fetch is not None and len(fetch) == 1:
                self.last_checked_comment = fetch[0]
            else:
                self.last_checked_comment = time()  # new sub
                self.c.execute(
                    "INSERT INTO tsb_repliedbot (subreddit_name, last_checked_comment) VALUES ('%s', to_timestamp(%d) AT TIME ZONE 'UTC')"
                    % (self.subreddit_name, self.last_checked_comment)
                )
        else:
            self.logger.error(f"{self.agent} | Launch Failed - Exiting")

    def cycle(self):
        try:
            self.checkDMs()
            self.checkNewComments()
        except UnicodeEncodeError as e:
            self.logger.error("{} | Caught UnicodeEncodeError! {}".format(self.me, str(e)))

        except prawcore.exceptions.InvalidToken:
            self.logger.warning("{} | API Token Error. Likely on reddits end. Issue self-resolves.".format(self.me))
            sleep(180)
        except prawcore.exceptions.BadJSON:
            self.logger.warning("{} | PRAW didn't get good JSON, probably reddit sending bad data due to site issues.".format(self.me))
            sleep(180)
        except (
            prawcore.exceptions.ResponseException,
            prawcore.exceptions.RequestException,
            prawcore.exceptions.ServerError,
            urllib3.exceptions.TimeoutError,
            Timeout,
        ):
            self.logger.warning("{} | HTTP Requests Error. Likely on reddits end due to site issues.".format(self.me))
            sleep(300)
        except praw.exceptions.APIException:
            self.logger.exception("{} | PRAW/Reddit API Error".format(self.me))
            sleep(30)
        except praw.exceptions.ClientException:
            self.logger.exception("{} | PRAW Client Error".format(self.me))
            sleep(30)
        except KeyboardInterrupt:
            self.logger.info("{} | Caught KeyboardInterrupt - Exiting".format(self.me))
            sys.exit()
        except Exception:
            self.logger.exception("{} | General Exception - Sleeping 5 min".format(self.me))
            sleep(300)
