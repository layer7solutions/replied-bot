import sys
import threading

from RepliedBot import RepliedBot

# See RepliedBot.py for default values
# TEXT is the only required field (error when missing)

DTG_CONFIG = {
    "FLAIR": True,
    "FLAIR_COUNT": True,
    "FLAIR_TEXT": " // Bungie Replied",
    "TRUNCATELEN": 150,
    "TEXT": "This is a list of links to comments made by Bungie employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "Verified-Bungie-Employee",
    "LOCK_COMMENT": True
}

FORTNITEBR_CONFIG = {
    "TRUNCATELEN": 400,
    "TEXT": "This is a list of links to comments made by Epic employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "epic",
}

FORTNITE_CONFIG = {
    "TRUNCATELEN": 400,
    "TEXT": "This is a list of links to comments made by Epic employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "epic",
}

DISCORDAPP_CONFIG = {
    "TEXT": "This is a list of links to comments made by Discord Staff in this thread:",
    "TRIGGER_FLAIR_CLASS": "discordstaff",
}

ROCKETLEAGUE_CONFIG = {
    "TEXT": "This is a list of links to comments made by Psyonix Staff in this thread:",
    "TRIGGER_FLAIR_CLASS": "staff",
}

STARWARSBATTLEFRONT_CONFIG = {
    "TRUNCATELEN": 150,
    "TEXT": "This is a list of links to comments made by DICE developers in this thread:",
    "TRIGGER_FLAIR_CLASS": "developer",
}

SWSQUADRONS_CONFIG = STARWARSBATTLEFRONT_CONFIG
SWSQUADRONS_CONFIG["TEXT"] = "This is a list of links to comments made by developers in this thread:"

FALLENORDER_CONFIG = {
    "TRUNCATELEN": 150,
    "TEXT": "This is a list of links to comments made by Respawn developers in this thread:",
    "TRIGGER_FLAIR_CLASS": "developer",
}

SWGALAXYOFHEROES_CONFIG = {
    "FLAIR": True,
    "FLAIR_TEXT": " // CG Replied",
    "TRUNCATELEN": 150,
    "TEXT": "This is a list of comments made by the EA developers in this thread:",
    "TRIGGER_FLAIR_CLASS": "dev",
}

FFXIV_CONFIG = {
    "FLAIR": True,
    "FLAIR_TEXT": " - Dev Response",
    "TEXT": "This is a list of comments made by the Square Enix developers in this thread:",
    "TRIGGER_FLAIR_CLASS": "square-enix",
}

FORTNITECOMPETITIVE_CONFIG = {
    "TRUNCATELEN": 400,
    "TEXT": "This is a list of links to comments made by Epic employees in this thread:",
    "TRIGGER_FLAIR_TEXT": ":epic:",
}

ATG_CONFIG = {
    "TRUNCATELEN": 150,
    "TEXT": "This is a list of links to comments made by BioWare employees in this thread:",
    "TRIGGER_FLAIR_CLASS": ("bwstaff", "eastaff"),
}

FO76_CONFIG = {
    "FLAIR": True,
    "FLAIR_COUNT": True,
    "FLAIR_TEXT": " // Bethesda Replied",
    "TRUNCATELEN": 150,
    "TEXT": "This is a list of links to comments made by Bethesda employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "bethesda",
}

CLASHROYALE_CONFIG = {
    "TRUNCATELEN": 150,
    "TEXT": "This is a list of links to comments made by Supercell in this thread:",
    "TRIGGER_FLAIR_CLASS": "SuperCell",
}

MAGICARENA_CONFIG = {
    "TRUNCATELEN": 250,
    "TEXT": "This is a list of links to comments made by WotC Employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "WotcAnimated",
}

BATTLEFIELDV_CONFIG = {
    "FLAIR": True,
    "FLAIR_COUNT": True,
    "FLAIR_TEXT": "DICE Replied // ",
    "FLAIR_TEXT_ADD_TO_END": False,
    "TEXT": "This is a list of links to comments made by DICE in this thread:",
    "TRIGGER_FLAIR_CLASS": "dice",
}

APEXLEGENDS_CONFIG = {
    "TEXT": "This is a list of links to comments made by Respawn developers in this thread:",
    "TRIGGER_FLAIR_TEXT": ":respawn:",
}

PLAYARK_CONFIG = {
    "FLAIR": True,
    "FLAIR_COUNT": True,
    "FLAIR_TEXT": "// Wildcard Replied",
    "FLAIR_CSS_FUNC": lambda flair_css: "wcreplied",
    "TEXT": "This is a list of links to comments made by Studio Wildcard in this thread:",
    "TRIGGER_FLAIR_CLASS": "modflair",
}

ROH_CONFIG = {
    "FLAIR": True,
    "FLAIR_COUNT": True,
    "FLAIR_TEXT": " // Wingjoy Replied",
    "MINCOMMENTLEN": 11,
    "TEXT": "This is a list of links to comments made by Wingjoy employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "wingjoydev",
}

COC_CONFIG = {
    "FLAIR_TEXT": "SUPERCELL RESPONSE",
    "TEXT": "This is a list of links to comments made by Supercell employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "supercell",
}

OUTRIDERS_CONFIG = {
    "FLAIR": True,
    "FLAIR_COUNT": True,
    "FLAIR_TEXT": " // Dev Replied",
    "TEXT": "This is a list of comments made by the developers in this thread:",
    "TRIGGER_FLAIR_CLASS": ("10", "11"),
}

CHIVALRY2_CONFIG = {
    "FLAIR": True,
    "FLAIR_COUNT": True,
    "FLAIR_TEXT": " // Torn Banner Replied",
    "TRUNCATELEN": 150,
    "TEXT": "This is a list of links to comments made by Torn Banner employees in this thread:",
    "TRIGGER_FLAIR_TEXT": ":tbs_logo_128x128:",
}

DEADBYDAYLIGHT_CONFIG = {
    "FLAIR": True,
    "FLAIR_TEXT": " | BHVR replied",
    "TEXT": "This is a list of links to comments made by Behaviour Interactive employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "developer",
}

DAYZ_CONFIG = {
    "TEXT": "This is a list of links to comments made by the Developers in this thread:",
    "TRIGGER_FLAIR_CLASS": "devs",
    "LOCK_COMMENT": True,
}

GHOSTRECON_CONFIG = {
    "FLAIR": True,
    "FLAIR_TEXT": " // Ubi-Response",
    "FLAIR_CSS_FUNC": lambda flair_css: "Ubiresponse",
    "TEXT": "This is a list of links to comments made by Ubisoft employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "ubisoft",
    "LOCK_COMMENT": True,
}

DAUNTLESS_CONFIG = {
    "FLAIR": True,
    "FLAIR_COUNT": True,
    "FLAIR_TEXT": " // PHX Labs replied",
    "TEXT": "This is a list of links to comments made by Phoenix Labs employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "phoenix-labs",
    "LOCK_COMMENT": True,
}

BATTLELEGION_CONFIG = {
    "FLAIR": True,
    "FLAIR_TEXT": " | Traplight Reply",
    "TEXT": "This is a list of comments from Traplight Staff in this thread:",
    "TRIGGER_FLAIR_TEXT": "Traplight",
    "LOCK_COMMENT": True,
}

BUKROBLOX_CONFIG = {
    "TEXT": "This is a list of links to comments made by Group Staff in this thread:",
    "TRIGGER_FLAIR_CLASS": "ilikepancakes",
}

FREENAS_CONFIG = {
    "FLAIR": True,
    "FLAIR_COUNT": True,
    "FLAIR_TEXT": "iXsystems Replied",
    "TEXT": "This is a list of links to comments made by iXsystems employees in this thread:",
    "TRIGGER_FLAIR_TEXT": "iXsystems",
}

HALO_CONFIG = {
    "FLAIR": True,
    "FLAIR_TEXT": "Halo Studios Response",
    "FLAIR_TEXT_REPLACE": True,
    "FLAIR_CSS_FUNC": lambda flair_css: "343",
    "FLAIR_ID": "0ede04d0-857c-11e5-80ef-0e68e762d2b5",
    "TEXT": "This is a list of links to comments made by Halo Studios employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "343dev",
    "REPLACE_STICKY_COMMENT": True,
}

REDDITMOBILE_CONFIG = {
    "TEXT": "This is a list of links to comments made by Reddit Admins in this thread:",
    "TRIGGER_FLAIR_CLASS": "admin",
}

HOTS_CONFIG = {
    "TEXT": "This is a list of links to comments made by Blizzard HotS developers in this thread:",
    "TRIGGER_FLAIR_CLASS": "blizzard",
    "LOCK_COMMENT": True,
}

TTA_CONFIG = {
    "FLAIR": True,
    "FLAIR_TEXT": "// TGM Replied",
    "FLAIR_CSS_FUNC": lambda flair_css: "DevReply",
    "TEXT": "This is a list of links to comments made by TGM employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "TGM",
}

RUMBLEVERSE_CONFIG = {
    "FLAIR": True,
    "FLAIR_TEXT": "// Iron Galaxy Replied",
    "FLAIR_CSS_FUNC": lambda flair_css: "DevReply",
    "TEXT": "This is a list of links to comments made by Iron Galaxy employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "Dev",
}

NASA_CONFIG = {
    "TEXT": "This is a list of links to comments made by NASA's official social media team in this thread:",
    "TRIGGER_FLAIR_CLASS": "NASA-Official",
    "LOCK_COMMENT": True
}

THECYCLEFRONTIER_CONFIG = {
    "FLAIR": True,
    "FLAIR_COUNT": True,
    "FLAIR_TEXT": " // YAGER Replied",
    "TRUNCATELEN": 150,
    "TEXT": "This is a list of links to comments made by YAGER employees in this thread:",
    "TRIGGER_FLAIR_CLASS": "YAGER-Staff",
    "LOCK_COMMENT": True,
}

PHASMOPHOBIAGAME_CONFIG = {
    "FLAIR": True,
    "FLAIR_COUNT": True,
    "FLAIR_TEXT": " // Kinetic Games Replied",
    "TRUNCATELEN": 150,
    "TEXT": "Kinetic Games has replied to this thread, see their comment(s) here:",
    "TRIGGER_FLAIR_CLASS": ("KG-Developer", "KG-Community-Manager"),
    "LOCK_COMMENT": True,
}

LAYER7_DEV_CONFIG = {
    "TEXT": "This is a list of links to comments made by Layer 7 developers in this thread:",
    "TRIGGER_FLAIR_CLASS": "layer7dev",
    "LOCK_COMMENT": True,
}

TTGMBOT_DEV_CONFIG = {
    "FLAIR": True,
    "FLAIR_COUNT": True,
    "FLAIR_TEXT": "// Test replies",
    "FLAIR_CSS_FUNC": lambda flair_css: flair_css
    if flair_css is not None and flair_css.startswith("bw")
    else "bw" + flair_css,
    "MINCOMMENTLEN": 5,
    "TEXT": "This is a list of links to comments made by test developers in this thread:",
    "TRIGGER_ON_OP": True,
    "LOCK_COMMENT": True,
}


def loop(bot):
    while True:
        try:
            if bot.canLaunch:
                bot.cycle()
        except KeyboardInterrupt:
            print("Caught Keyboard Interrupt")
            sys.exit()


def main():
    bots = []

    # RepliedBot('Subreddit', 'AgentOf', ConfigFile)

    # ~~~ Production ~~~ #

    # r/DestinyTheGame
    bots.append(RepliedBot("DestinyTheGame", "dtgbot", DTG_CONFIG))

    # r/Fortnite
    bots.append(RepliedBot("FortNite", "fortnite_repliedbot", FORTNITE_CONFIG))

    # r/FortniteBR
    bots.append(RepliedBot(
        "FortNiteBR", "fortnitebr_repliedbot", FORTNITEBR_CONFIG))

    # r/FortniteMobile
    bots.append(
        RepliedBot("FortNiteMobile", "fortnitebr_repliedbot",
                   FORTNITEBR_CONFIG)
    )

    # r/FortniteCreative
    bots.append(
        RepliedBot("FortniteCreative",
                   "fortnitebr_repliedbot", FORTNITEBR_CONFIG)
    )

    # r/DiscordApp
    bots.append(RepliedBot("DiscordApp", "DiscordAppMods", DISCORDAPP_CONFIG))

    # r/RocketLeague
    bots.append(RepliedBot("RocketLeague",
                           "PsyonixCommentBot", ROCKETLEAGUE_CONFIG))

    # r/StarWarsBattlefront
    bots.append(
        RepliedBot(
            "StarWarsBattlefront",
            "StarWarsBattlefront_repliedbot",
            STARWARSBATTLEFRONT_CONFIG,
        )
    )

    # r/StarWarsSquadrons
    bots.append(
        RepliedBot(
            "StarWarsSquadrons",
            "StarWarsBattlefront_repliedbot",
            SWSQUADRONS_CONFIG,
        )
    )

    # r/FallenOrder
    bots.append(
        RepliedBot("FallenOrder", "StarWarsBattlefront_repliedbot",
                   FALLENORDER_CONFIG)
    )

    # r/SWGalaxyOfHeroes
    bots.append(
        RepliedBot("SWGalaxyOfHeroes", "SWGOH_Bot_repliedbot",
                   SWGALAXYOFHEROES_CONFIG)
    )

    # r/ffxiv
    bots.append(RepliedBot(
        "ffxiv", "FFXIV_DevTracker_repliedbot", FFXIV_CONFIG))

    # r/FortniteCompetitive
    bots.append(
        RepliedBot(
            "FortniteCompetitive",
            "TTV_EpicComments_repliedbot",
            FORTNITECOMPETITIVE_CONFIG,
        )
    )

    # r/AnthemTheGame
    bots.append(RepliedBot("AnthemTheGame", "ATG_repliedbot", ATG_CONFIG))
    bots.append(RepliedBot("DylanTheGame", "ATG_repliedbot", ATG_CONFIG))

    # r/fo76
    # r/fo76PTS
    # r/Starfield
    bots.append(RepliedBot("fo76", "FO76_repliedbot", FO76_CONFIG))
    bots.append(RepliedBot("fo76PTS", "FO76_repliedbot", FO76_CONFIG))
    bots.append(RepliedBot("Starfield", "FO76_repliedbot", FO76_CONFIG))

    # r/ClashRoyale
    bots.append(
        RepliedBot("ClashRoyale", "rClashRoyale_repliedbot",
                   CLASHROYALE_CONFIG)
    )

    # r/MagicArena
    bots.append(RepliedBot(
        "MagicArena", "rMagicArena_repliedbot", MAGICARENA_CONFIG))

    # r/BattlefieldV
    # r/battlefield2042
    bots.append(
        RepliedBot("BattlefieldV", "rBattlefieldV_repliedbot",
                   BATTLEFIELDV_CONFIG)
    )
    bots.append(
        RepliedBot("battlefield2042", "rBattlefield2042_repliedbot",
                   BATTLEFIELDV_CONFIG)
    )

    # r/ApexLegends
    bots.append(
        RepliedBot("ApexLegends", "rApexLegends_repliedbot",
                   APEXLEGENDS_CONFIG)
    )

    # r/playARK
    bots.append(RepliedBot("playARK", "rplayARK_repliedbot", PLAYARK_CONFIG))

    # r/RiseOfHeroesRoH
    bots.append(RepliedBot("RiseOfHeroesRoH",
                           "rRiseOfHeroesRoH_repliedbot", ROH_CONFIG))

    # r/ClashOfClans
    bots.append(RepliedBot("ClashOfClans", "coc_repliedbot", COC_CONFIG))

    # r/Outriders
    bots.append(RepliedBot("Outriders", "outriders_repliedbot", OUTRIDERS_CONFIG))

    # r/Chivalry2
    bots.append(RepliedBot("Chivalry2", "rChivalry2_repliedbot", CHIVALRY2_CONFIG))

    # r/GhostRecon
    bots.append(RepliedBot("GhostRecon", "tsb_repliedbot", GHOSTRECON_CONFIG))

    # r/Dauntless
    bots.append(RepliedBot("dauntless", "rDauntless_repliedbot", DAUNTLESS_CONFIG))

    # r/BattleLegion
    bots.append(RepliedBot("battlelegion",
                           "tsb_repliedbot", BATTLELEGION_CONFIG))

    # r/bukroblox
    bots.append(RepliedBot("bukroblox", "tsb_repliedbot", BUKROBLOX_CONFIG))

    # r/freenas
    bots.append(RepliedBot("freenas", "tsb_repliedbot", FREENAS_CONFIG))

    # r/halo
    bots.append(RepliedBot("halo", "halo_repliedbot", HALO_CONFIG))

    # r/redditmobile
    bots.append(RepliedBot("redditmobile", "tsb_repliedbot", REDDITMOBILE_CONFIG))

    # r/heroesofthestorm
    bots.append(RepliedBot("heroesofthestorm", "hots_repliedbot", HOTS_CONFIG))

    # r/TeeTimeAdventures
    bots.append(RepliedBot("shankstars", "tgm_repliedbot", TTA_CONFIG))

    # r/rumbleverse
    bots.append(RepliedBot("rumbleverse", "rRumbleverse_repliedbot", RUMBLEVERSE_CONFIG))

    # r/nasa
    bots.append(RepliedBot("nasa", "tsb_repliedbot", NASA_CONFIG))

    # r/thecyclefrontier
    bots.append(RepliedBot("thecyclefrontier", "thecyclefrontier_repliedbot", THECYCLEFRONTIER_CONFIG))

    # r/PhasmophobiaGame
    bots.append(RepliedBot("PhasmophobiaGame", "tsb_repliedbot", PHASMOPHOBIAGAME_CONFIG))

    # ~~~ Development ~~~ #

    # r/Layer7Dev
    bots.append(RepliedBot("Layer7Dev", "tsb_repliedbot", LAYER7_DEV_CONFIG))

    # r/ttgmbot
    # bots = [RepliedBot("ttgmbot", "dtgbot", TTGMBOT_DEV_CONFIG)]

    # RepliedBot('Subreddit', 'AgentOf', ConfigFile)

    for bot in bots:
        threading.Thread(target=loop, args=(bot,)).start()
